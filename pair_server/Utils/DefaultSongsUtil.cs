﻿using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Utils
{
    public static class DefaultSongsUtil
    {
        public static ICollection<SongModel> DefaultSongs(ICollection<SongModel> songs)
        {
            try
            {
                int[] intKinds = (int[])Enum.GetValues(typeof(Kind));
                int[] intFormats = (int[])Enum.GetValues(typeof(Format));

                foreach (var song in songs)
                {
                    if (!intKinds.Contains(song.Kind))
                    {
                        song.Kind = 0;
                    }
                    if (!intFormats.Contains(song.Format))
                    {
                        song.Format = 0;
                    }
                }
            }
            catch
            {
                return songs;
            }
            
            return songs;
        }
    }
}
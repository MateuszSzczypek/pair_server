﻿using pair_server.Data.DbModels;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace pair_server.Data
{
    public class KaraokeContext : DbContext
    {
        public KaraokeContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<BackupSong> BackupSongs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Song>().HasRequired<User>(v => v.User).WithMany(c => c.Songs).HasForeignKey(k => k.UserID).WillCascadeOnDelete(false);
        }
    }
}
﻿using pair_server.Data.DbModels;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace pair_server.Data
{
    public class KaraokeInitializer : DropCreateDatabaseIfModelChanges<KaraokeContext>
    {
        protected override void Seed(KaraokeContext db)
        {
            #region server
            User server = new User
            {
                IsActive = true,
                Address = "127.0.0.1:5000",
                Login = "Server",
                Songs = new List<Song>()
            };

            db.Users.Add(server);
            db.SaveChanges();
            #endregion
        }
    }
}
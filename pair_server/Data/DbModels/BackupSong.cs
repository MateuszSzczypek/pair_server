﻿using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Data.DbModels
{
    public class BackupSong
    {
        public int ID { get; set; }
        public byte[] Content { get; set; }
        public int SongID { get; set; }
        public Song Song { get; set; }
    }
}
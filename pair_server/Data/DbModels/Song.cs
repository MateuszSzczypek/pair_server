﻿using pair_server.Data.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Models
{
    public enum Kind
    {
        Inny,
        Disco,
        Techno,
        Country,
        Punk,
        Pop,
        Metal,
        Rock
    }

    public enum Format
    {
        Inny,
        spiew,
        MP3,
        MP4,
        avi
    }

    public class Song
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public Kind Kind { get; set; }
        public float Rating { get; set; }
        public Format Format { get; set; }
        public DateTime LastModification { get; set; }
        public float Size { get; set; }
        public TimeSpan Length { get; set; }
        public string Album { get; set; }
        public int Year { get; set; }
        public string Performer { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
    }
}
﻿using pair_server.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pair_server.Data.DbModels
{
    public class User
    {
        [Key]
        public int ID { get; set; } = -1;
        [Index(IsUnique = true), Required, StringLength(500)]
        public string Login { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Song> Songs { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Models
{
    public class SongModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Kind { get; set; }
        public float Rating { get; set; }
        public int Format { get; set; }
        public string LastModification { get; set; }
        public float Size { get; set; }
        public string Length { get; set; }
        public string Album { get; set; }
        public int Year { get; set; }
        public string Performer { get; set; }

        public int UserID { get; set; }
        public UserModel User { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Models
{
    public class UserModel
    {
        public int ID { get; set; } = -1;
        public string Login { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<SongModel> Songs { get; set; }
    }
}
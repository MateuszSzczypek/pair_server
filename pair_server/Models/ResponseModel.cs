﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace pair_server.Models
{
    public class ResponseModel
    {
        public object Data { get; set; }

        public HttpStatusCode Status { get; set; }
        public string Response { get; set; }

    }
}
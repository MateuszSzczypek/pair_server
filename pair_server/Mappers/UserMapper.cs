﻿using pair_server.Data.DbModels;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Mappers
{
    public static class UserMapper
    {
        public static User MapToDB(UserModel userModel)
        {
            return new User
            {
                ID = userModel.ID,
                Address = userModel.Address,
                IsActive = userModel.IsActive,
                Login = userModel.Login,
                Songs = userModel.Songs == null ? new List<Song>() : userModel.Songs.Select(s =>
                   new Song()
                   {
                       ID = s.ID,
                       Album = s.Album,
                       Format = (Format) s.Format,
                       Kind = (Kind) s.Kind,
                       LastModification = DateTime.ParseExact(s.LastModification, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture),
                       Length = TimeSpan.Parse(s.Length),
                       Performer = s.Performer,
                       Rating = s.Rating,
                       Size = s.Size,
                       Title = s.Title,
                       Year = s.Year,
                       UserID = userModel.ID
                   }
                ).ToList()
            };
        }

        public static UserModel MapToModel(User user)
        {
            return new UserModel
            {
                ID = user.ID,
                Address = user.Address,
                IsActive = user.IsActive,
                Login = user.Login,
                Songs = user.Songs == null ? new List<SongModel>() : user.Songs.Select(s =>
                   new SongModel()
                   {
                       ID = s.ID,
                       Album = s.Album,
                       Format = (int) s.Format,
                       Kind = (int) s.Kind,
                       LastModification = s.LastModification.ToString("yyyy - MM - dd HH: mm:ss.fff"),
                       Length = string.Format("{0:hh\\:mm\\:ss}", s.Length),
                       Performer = s.Performer,
                       Rating = s.Rating,
                       Size = s.Size,
                       Title = s.Title,
                       Year = s.Year,
                       UserID = user.ID
                   }
                ).ToList()
            };
        }
    }
}
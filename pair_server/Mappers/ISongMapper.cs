﻿using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pair_server.Mappers
{
    interface ISongMapper
    {
        Song MapToDB(SongModel userModel);
        SongModel MapToModel(Song user);
    }
}

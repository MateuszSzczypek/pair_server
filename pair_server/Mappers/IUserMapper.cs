﻿using pair_server.Data.DbModels;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pair_server.Mappers
{
    interface IUserMapper
    {
        User MapToDB(UserModel userModel);
        UserModel MapToModel(User user);
    }
}

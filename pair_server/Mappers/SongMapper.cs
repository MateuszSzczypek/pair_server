﻿using pair_server.Data;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pair_server.Mappers
{
    public static class SongMapper
    {
       // private static KaraokeContext db = new KaraokeContext();

        public static Song MapToDB(SongModel songModel)
        {
            KaraokeContext db = new KaraokeContext();

            Song s = new Song
            {
                ID = songModel.ID,
                Album = songModel.Album,
                Format = (Format)songModel.Format,
                Kind = (Kind)songModel.Kind,
                LastModification = DateTime.ParseExact(songModel.LastModification, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture),
                Length = TimeSpan.Parse(songModel.Length),
                Performer = songModel.Performer,
                Rating = songModel.Rating,
                Size = songModel.Size,
                Title = songModel.Title,
                Year = songModel.Year,
                UserID = songModel.UserID,
                User = db.Users.FirstOrDefault(u => u.ID == songModel.UserID)
            };

            db.Dispose();

            return s;
        }

        public static SongModel MapToModel(Song song)
        {
            KaraokeContext db = new KaraokeContext();

            SongModel sm = new SongModel
            {
                ID = song.ID,
                Album = song.Album,
                Format = (int)song.Format,
                Kind = (int)song.Kind,
                LastModification = song.LastModification.ToString("yyyy - MM - dd HH: mm:ss.fff"),
                Length = string.Format("{0:hh\\:mm\\:ss}", song.Length),
                Performer = song.Performer,
                Rating = song.Rating,
                Size = song.Size,
                Title = song.Title,
                Year = song.Year,
                UserID = song.UserID,
                User = db.Users.Where(u => u.ID == song.UserID).Select(u => new UserModel { ID = song.UserID, Address = u.Address, IsActive = u.IsActive, Login = u.Login }).FirstOrDefault()
            };

            db.Dispose();

            return sm;
        }
    }
}
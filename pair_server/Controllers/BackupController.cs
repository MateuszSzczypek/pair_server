﻿using Newtonsoft.Json.Linq;
using pair_server.Data;
using pair_server.Data.DbModels;
using pair_server.Mappers;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pair_server.Controllers
{
    public class BackupController : ApiController
    {
        private KaraokeContext db = new KaraokeContext();

        // GET api/backup
        [System.Web.Http.HttpGet]
        public ICollection<BackupSong> Get()
        {
            return db.BackupSongs.ToList();
        }

        #region backup songs

        // Ktos pobiera od kogos piosenke? Strzel tutaj piosenke o jakim ID i od kogo
        //POST api/backup/{SongID}{login}
        public void Post(int SongID, string login)
        {
            Get(SongID, login);
        }

        // GET api/backup?{SongID}{login}
        [System.Web.Http.HttpGet]
        public void Get(int SongID, string login)
        {
            try
            {
                User userWithSong = db.Users.Where(u => u.Login == login).FirstOrDefault();
                Song songToDownload = db.Songs.Where(s => s.ID == SongID).FirstOrDefault();

                BackupSong savedSong = new BackupSong();
                byte[] songByte;

                //TUTAJ STRZELAM DO KLIENTA PO PLIK, JAKIS ENDPOINT? OCZEKUJE TABLICY BAJTOW, KTORA BEDZIE TA PIOSENKA
                string endpoint = "/rybkowy";

                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = Task.Run(() => client.PostAsync(userWithSong.Address + endpoint, new StringContent("", Encoding.UTF8, "application/json"))).Result;

                    songByte = Task.Run(() => response.Content.ReadAsByteArrayAsync()).Result;
                }

                savedSong = new BackupSong { Content = songByte, Song = songToDownload };
                db.SaveChangesAsync();
            }
            catch
            {

            }
        }

        #endregion


        // GET api/backup?{SongID}
        [System.Web.Http.HttpGet]
        public byte[] Get(int SongID)
        {
            BackupSong requestedSong;

            try
            {
                requestedSong = db.BackupSongs.Where(b => b.SongID == SongID).FirstOrDefault();
            }
            catch
            {
                return new byte[0];
            }
            return requestedSong.Content;
        }
    }
}
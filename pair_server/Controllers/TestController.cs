﻿using pair_server.Data;
using pair_server.Data.DbModels;
using pair_server.Mappers;
using pair_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pair_server.Controllers
{
    
    public class TestController : ApiController
    {
        private KaraokeContext db = new KaraokeContext();

        // GET: api/Test
        public string Get(int id)
        {
            return "duoa";
        }

        // GET: api/Test
        public List<User> Get()
        {
            return db.Users.ToList();
        }

        // POST api/Test
        [System.Web.Http.HttpPost]
        public ResponseModel Post([FromBody] UserModel userModel)
        {
            try
            {
                db.Users.Add(UserMapper.MapToDB(userModel));
                db.SaveChanges();

                return new ResponseModel { Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully logged in" };
            }
            catch
            {
                return new ResponseModel { Status = System.Net.HttpStatusCode.InternalServerError, Response = "An error has occured while processing your rquest" };
            }
        }
    }
}
﻿using pair_server.Data;
using pair_server.Data.DbModels;
using pair_server.Mappers;
using pair_server.Models;
using pair_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebGrease.Css.Extensions;

namespace pair_server.Controllers
{
    public class AuthController : ApiController
    {
        private KaraokeContext db = new KaraokeContext();
        
        // Login POST api/Auth
        [System.Web.Http.HttpPost]
        public ResponseModel Post([FromBody] UserModel userModel)
        {
            try
            {
                DefaultSongsUtil.DefaultSongs(userModel.Songs);

                db.Users.Add(UserMapper.MapToDB(userModel));
                db.SaveChanges();

                return new ResponseModel { Data = userModel, Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully registered" };
            }
            catch
            {
                return new ResponseModel { Status = System.Net.HttpStatusCode.InternalServerError, Response = "User with that login already exists" };
            }
        }


        // Login/Register PUT api/Auth
        [System.Web.Http.HttpPut]
        public ResponseModel Put([FromBody] UserModel userModel, string login = "")
        {
            object response = null;

            try
            {
                if (!db.Users.Where(u => u.Login.Equals(login)).Any())
                {
                    response = Post(userModel);
                }
                else
                {
                    response = new UsersController().Patch(userModel, login);
                }
                return (ResponseModel) response;
            }
            catch
            {
                return new ResponseModel { Data = ((ResponseModel)response).Data, Status = System.Net.HttpStatusCode.InternalServerError, Response = "An error has occured while processing your rquest" };
            }
        }

        // Login/Logout POST api/Auth
        [System.Web.Http.HttpPost]
        public ResponseModel Post([FromBody] UserModel userModel, string login)
        {
            try
            {
                User dbUser = db.Users.FirstOrDefault(b => b.Login.Equals(login));

                DefaultSongsUtil.DefaultSongs(userModel.Songs);
                User userToUpdate = UserMapper.MapToDB(userModel);

                if (dbUser == null)
                {
                    throw new Exception("User to logout not found");
                }
                
                dbUser.IsActive = userToUpdate.IsActive;
                
                db.SaveChanges();

                return new ResponseModel { Data = UserMapper.MapToModel(dbUser), Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully logout user" };
            }
            catch
            {
                return new ResponseModel { Data = userModel, Status = System.Net.HttpStatusCode.InternalServerError, Response = "An error has occured while processing your rquest" };
            }
        }
    }
}
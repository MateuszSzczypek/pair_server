﻿using pair_server.Data;
using pair_server.Data.DbModels;
using pair_server.Mappers;
using pair_server.Models;
using pair_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pair_server.Controllers
{
    public class UsersController : ApiController
    {
        private KaraokeContext db = new KaraokeContext();

        // GET api/users
        public ICollection<UserModel> Get()
        {
            return db.Users.Where(u => u.IsActive).ToArray().Select(m => UserMapper.MapToModel(m)).ToList();
        }


        // Register PATCH api/users
        [System.Web.Http.HttpPatch]
        public ResponseModel Patch([FromBody] UserModel userModel, string login)
        {
            try
            {
                User dbUser = db.Users.FirstOrDefault(b => b.Login.Equals(login));

                DefaultSongsUtil.DefaultSongs(userModel.Songs);
                User userToUpdate = UserMapper.MapToDB(userModel);

                if (dbUser == null)
                {
                    throw new Exception("User to update not found");
                }

                if (!String.IsNullOrEmpty(userToUpdate.Login) && dbUser.Login != userToUpdate.Login)
                    dbUser.Login = userToUpdate.Login;

                if (!String.IsNullOrEmpty(userToUpdate.Address) && dbUser.Address != userToUpdate.Address)
                    dbUser.Address = userToUpdate.Address;

                if (dbUser.IsActive != userToUpdate.IsActive)
                    dbUser.IsActive = userToUpdate.IsActive;

                if (userToUpdate.Songs.Any() && ((!dbUser.Songs.All(userToUpdate.Songs.Contains) && dbUser.Songs.Count() != userToUpdate.Songs.Count()))
                || userToUpdate.Songs.Count() != 0)
                {
                    List<Song> bulkUpdateHelperList = new List<Song>(dbUser.Songs);
                    foreach (var song in bulkUpdateHelperList)
                    {
                        db.Songs.Remove(song);
                    }

                    dbUser.Songs.Clear();
                    db.SaveChanges();

                    dbUser.Songs = userToUpdate.Songs;
                }

                db.SaveChanges();

                return new ResponseModel { Data = UserMapper.MapToModel(dbUser), Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully updated user" };
            }
            catch
            {
                return new ResponseModel { Data = userModel, Status = System.Net.HttpStatusCode.InternalServerError, Response = "An error has occured while processing your rquest" };
            }
        }
    }
}
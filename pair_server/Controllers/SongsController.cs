﻿using pair_server.Data;
using pair_server.Data.DbModels;
using pair_server.Mappers;
using pair_server.Models;
using pair_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace pair_server.Controllers
{
    public class SongsController : ApiController
    {
        private KaraokeContext db = new KaraokeContext();

        // GET api/songs
        public ICollection<SongModel> Get(string login = "")
        {
            if (string.IsNullOrEmpty(login))
            {
                return db.Songs.Where(u => u.User.IsActive).ToArray().Select(s => SongMapper.MapToModel(s)).ToList();
            }
            else
            {
                return db.Songs.Where(u => u.User.IsActive && login.Equals(u.User.Login)).ToArray().Select(s => SongMapper.MapToModel(s)).ToList();
            }
        }

        // POST api/songs
        [System.Web.Http.HttpPost]
        public ResponseModel Post([FromBody] ICollection<SongModel> songModels, string login)
        {
            try
            {
                DefaultSongsUtil.DefaultSongs(songModels);

                foreach (var song in songModels)
                {
                    Song tmp = SongMapper.MapToDB(song);
                    tmp.User = db.Users.FirstOrDefault(u => u.Login == login);

                    db.Songs.Add(tmp);
                    db.SaveChanges();
                }

                return new ResponseModel { Data = songModels, Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully added new songs to user" };
            }
            catch
            {
                return new ResponseModel { Status = System.Net.HttpStatusCode.InternalServerError, Response = "Song adding error." };
            }
        }

        // DELETE api/songs
        [System.Web.Http.HttpDelete]
        public ResponseModel Delete([FromBody] int[] songIDs, string login)
        {
            try
            {
                List<Song> songs = db.Songs.Where(s => songIDs.Contains(s.ID)).ToList();

                foreach (var song in songs)
                {
                    song.User = db.Users.FirstOrDefault(u => u.Login == login);
                    db.Songs.Remove(song);
                    db.SaveChanges();
                }

                return new ResponseModel { Data = songs, Status = System.Net.HttpStatusCode.OK, Response = "You have sucessfully removed songs from user" };
            }
            catch
            {
                return new ResponseModel { Status = System.Net.HttpStatusCode.InternalServerError, Response = "Song removing error." };
            }
        }
    }
}